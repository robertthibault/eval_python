import threading
import socket
import argparse
import os


class Server(threading.Thread):
    """
    Les données de connection
    """

    def __init__(self, host, port):
        super().__init__()
        self.connections = []
        self.host = host
        self.port = port

    def run(self):
        """
        Création su socket du serveur
        """
        socket_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        socket_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        socket_server.bind((self.host, self.port))

        socket_server.listen(1)
        print('Listening at', socket_server.getsockname())

        while True:
            # Accepte la connection
            sc, sockname = socket_server.accept()
            print('Accepted a new connection from {} to {}'.format(sc.getpeername(), sc.getsockname()))

            server_socket = ServerSocket(sc, sockname, self)
            server_socket.start()

            # Add thread to active connections
            self.connections.append(server_socket)
            print('Ready to receive messages from', sc.getpeername())

    def broadcast(self, message, source):
        """
        Envoie un message à tous les clients connectés sauf à l'expéditeur
        """
        for connection in self.connections:

            if connection.sockname != source:
                connection.send(message)

    def remove_connection(self, connection):
        """
        Supprime le thread de l'attribut de connexion
        """
        self.connections.remove(connection)


class ServerSocket(threading.Thread):
    """
    Prend en charge les communications avec un client connecté
    """

    def __init__(self, sc, sockname, server):
        super().__init__()
        self.sc = sc
        self.sockname = sockname
        self.server = server

    def run(self):
        """
        Reçoit les données du client connecté et diffuse le message à tous les autres clients.
        """
        while True:
            message = self.sc.recv(1024).decode('ascii')
            if message:
                print('{} says {!r}'.format(self.sockname, message))
                self.server.broadcast(message, self.sockname)
            else:
                # Le client a stoppé la connection
                print('{} has closed the connection'.format(self.sockname))
                self.sc.close()
                server.remove_connection(self)
                return

    def send(self, message):
        """
        Envoie un message au serveur
        """
        self.sc.sendall(message.encode('ascii'))


def exit(server):
    """
    Permet d'arrêter le serveur
    """
    while True:
        ipt = input('')
        if ipt == 'q':
            print('Closing all connections...')
            for connection in server.connections:
                connection.sc.close()
            print('Shutting down the server...')
            os._exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Chatroom Server')
    # Récupérer les informations pour lancer le serveur
    parser.add_argument('host', help='Interface the server listens at')
    parser.add_argument('-p', metavar='PORT', type=int, default=8080,
                        help='TCP port (default 8080)')
    args = parser.parse_args()

    # Créer et lancer le serveur
    server = Server(args.host, args.p)
    server.start()

    exit = threading.Thread(target=exit, args=(server,))
    exit.start()