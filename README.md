#Evaluation Python 2021

## Comment utiliser le Chat ? 
Lancer le serveur: ```python3 server.py <host> [-p PORT]```
(être à la racine du projet pur lancer depuis un terminal)

Arguments:
- ```host```: Hostname, ou une adresse IP.
- ```-p PORT```: le port TCP (défaut 8080)

Par exemple:

```bash
$ python3 server.py 127.0.0.1
```

Lancer en tant que client: ```python3 client.py <host> [-p PORT]```
(être à la racine du projet pur lancer depuis un terminal)

Arguments:
- ```host```: Hostname, ou une adresse IP.
- ```-p PORT```: le port TCP (défaut 8080)

Par exemple:

```bash
$ python3 client.py 127.0.0.1
```